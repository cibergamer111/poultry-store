﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;

using ServiceLayer.Interfaces;
using OutputDTO = DTO.Output;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IWatchCatalogUseCase watchCatalogUseCase;

        public ProductController(IWatchCatalogUseCase watchCatalogUseCase)
        {
            this.watchCatalogUseCase = watchCatalogUseCase;
        }

        // GET: <ProductController>
        [HttpGet]
        public IEnumerable<OutputDTO::Product> Get()
        {
            Log.Information("Getting all products.");
            return watchCatalogUseCase.GetAllProducts();
        }

        // GET: <ProductController>/5
        [HttpGet("{categoryId}")]
        public IEnumerable<OutputDTO::Product> GetProductsByCategoryId(int categoryId)
        {
            Log.Information("Getting products by categoryId.");
            return watchCatalogUseCase.GetProductsByCategoryId(categoryId);
        }
    }
}

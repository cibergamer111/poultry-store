﻿using System;
using System.Security.Claims;

namespace API.Controllers
{
    public static class ControllerExtensions
    {
        public static Guid GetNameId(this ClaimsPrincipal user)
        {
            if (user.Identity.IsAuthenticated
                && user.HasClaim(c => c.Type == ClaimTypes.NameIdentifier))
            {
                var claimValue = user.FindFirstValue(ClaimTypes.NameIdentifier);
                return Guid.Parse(claimValue);
            }

            throw new ApplicationException($"User id was not found in claims.");
        }
    }
}

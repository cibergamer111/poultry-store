﻿using System;
using Microsoft.AspNetCore.Mvc;
using ServiceLayer.Interfaces;
using InputDTO = DTO.Input;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ShoppingCartController : ControllerBase
    {
        private readonly IBuyProductUseCase buyProductUseCase;

        public ShoppingCartController(IBuyProductUseCase buyProductUseCase)
        {
            this.buyProductUseCase = buyProductUseCase;
        }

        [HttpPost]
        public IActionResult AddProductToCart([FromBody] InputDTO.AddToCart input)
        {
            try
            {
                var userId = User.GetNameId();
                buyProductUseCase.AddProductToCart(input.ProductId, input.Amount, userId);

                return Ok();
            }
            catch (ApplicationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}

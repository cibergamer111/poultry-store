﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using ServiceLayer.Interfaces;
using OutputDTO = DTO.Output;

namespace API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class CategoryController : ControllerBase
    {
        private readonly IWatchCatalogUseCase watchCatalogUseCase;

        public CategoryController(IWatchCatalogUseCase watchCatalogUseCase)
        {
            this.watchCatalogUseCase = watchCatalogUseCase;
        }


        // GET: <CategoryController>
        [HttpGet]
        public IEnumerable<OutputDTO::Category> Get()
        {
            Log.Information("Getting all categoryes.");
            return watchCatalogUseCase.GetCategories();
        }

        //// GET <CategoryController>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST <CategoryController>
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT <CategoryController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE <CategoryController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}

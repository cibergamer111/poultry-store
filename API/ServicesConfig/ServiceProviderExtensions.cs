﻿using DAL;
using DAL.Repositories;
using DAL.Specifications;
using Microsoft.Extensions.DependencyInjection;
using ServiceLayer.Assistants;
using ServiceLayer.CustomerUseCases;
using ServiceLayer.Gateways;
using ServiceLayer.Gateways.Repositories;
using ServiceLayer.Gateways.Specifications;
using ServiceLayer.Interfaces;

namespace API.ServicesConfig
{
    public static class ServiceProviderExtensions
    {
        public static void AddUnitOfWorkService(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        public static void AddUseCaseServices(this IServiceCollection services)
        {
            services.AddScoped<IWatchCatalogUseCase, WatchCatalogUseCase>();
            services.AddScoped<IBuyProductUseCase, BuyProductUseCase>();
        }

        public static void AddGatewayServices(this IServiceCollection services)
        {
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IShoppingCartRepository, ShoppingCartRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();

            services.AddScoped<IProductSpecs, ProductSpecs>();
        }

        public static void AddAssistantServices(this IServiceCollection services)
        {
            services.AddScoped<ICustomerAssistant, CustomerAssistant>();
        }
    }
}

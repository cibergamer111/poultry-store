﻿
using BusinessLayer;
using BusinessLayer.Interfaces;
using ServiceLayer.Gateways.Repositories;

namespace DAL.Repositories
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationContext context)
            : base(context, context.Orders)
        {
        }

        public void AddOrder(IOrder order)
        {
            throw new System.NotImplementedException();
        }

        public Order RetrieveById(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}

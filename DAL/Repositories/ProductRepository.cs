﻿using System.Collections.Generic;
using System.Linq;

using BusinessLayer;
using BusinessLayer.Interfaces;
using Serilog;
using ServiceLayer.Gateways.Repositories;

namespace DAL.Repositories
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(ApplicationContext context)
            : base(context, context.Products)
        {
        }

        public IEnumerable<IProduct> RetrieveAllProducts()
        {
            //Use only once, for creating some test rows in db.
            //CreateTestProducts();

            var products = _context.Products.ToList();

            Log.Debug("Retrieving products from db. {@productList}", products);
            return products;
        }

        public Product RetrieveById(int id)
        {
            return _context.Products.SingleOrDefault(p => p.Id == id);
        }

        private void CreateTestProducts()
        {
            //List<Product> products = new()
            //{
            //    new() { Name = "Несушка рыжая февральская", InStock = "5", Price = "350 ₽", Image = "img/gallery2.jpg" },
            //    new() { Name = "Цыплята", InStock = "45", Price = "150 ₽", Image = "img/gallery4.jpg" },
            //    new() { Name = "Бройлеры", InStock = "30", Price = "500 ₽", Image = "img/cart-broyler.jpg" },
            //    new() { Name = "Хер лысый", InStock = "1", Price = "Бесценно", Image = "img/gallery3.jpg" }
            //};
            List<Product> products = new()
            {
                new Product("Несушка рыжая февральская", 350m, 5),
                new Product("Цыплята", 150, 45),
                new Product("Бройлеры", 500, 30),
                new Product("Хер лысый", 0m, 1)
            };

            foreach (var product in products)
            {
                _context.Products.Add(product);
            }

            _context.SaveChanges();

        }
    }
}

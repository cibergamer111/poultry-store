﻿using System;
using System.Collections.Generic;
using System.Linq;

using BusinessLayer;
using BusinessLayer.Interfaces;
using Serilog;
using ServiceLayer.Gateways.Repositories;

namespace DAL.Repositories
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository(ApplicationContext context)
            : base(context, context.Categories)
        {
        }

        public IEnumerable<ICategory> RetrieveAllCategories()
        {
            //Use only once, for creating some test rows in db.
            //CreateTestCategoryes();

            var categoryes = _context.Categories.ToList();

            Log.Debug("Retrieving categoryes from db. {@categoryList}", categoryes);
            return categoryes;
        }

        public Category RetrieveById(int id)
        {
            throw new NotImplementedException();
        }

        private void CreateTestCategoryes()
        {
            List<Category> categoryes = new()
            {
                new Category("Бройлеры", "img/cart-broyler.jpg"),
                new Category("Несушки", "img/cart-nesushka.jpg"),
                new Category("Яйца", "img/gallery6.jpg")
            };

            foreach (var cat in categoryes)
            {
                _context.Categories.Add(cat);
            }

            _context.SaveChanges();

        }
    }
}

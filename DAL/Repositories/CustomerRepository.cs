﻿using System;
using System.Linq;
using BusinessLayer;
using ServiceLayer.Gateways.Repositories;

namespace DAL.Repositories
{
    public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(ApplicationContext context)
            : base(context, context.Customers)
        {
        }

        public Customer RetrieveById(int id)
        {
            return _context.Customers.SingleOrDefault(c => c.Id == id);
        }

        public void AddCustomer(Customer customer)
        {
            _context.Customers.Add(customer);
        }

        public Customer RetrieveByUserId(Guid userId)
        {
            return _context.Customers.FirstOrDefault(c => c.UserId == userId);
        }
    }
}

﻿
using BusinessLayer;
using BusinessLayer.Interfaces;
using ServiceLayer.Gateways.Repositories;

namespace DAL.Repositories
{
    public class ShoppingCartRepository : RepositoryBase<ShoppingCart>, IShoppingCartRepository
    {
        public ShoppingCartRepository(ApplicationContext context)
            : base(context, context.ShoppingCarts)
        {
        }

        public void DeleteShoppingCart(IShoppingCart shoppingCart)
        {
            throw new System.NotImplementedException();
        }

        public ShoppingCart RetrieveById(int id)
        {
            throw new System.NotImplementedException();
        }

        public IShoppingCart RetrieveShoppingCartByUserId(int userId)
        {
            throw new System.NotImplementedException();
        }
    }
}

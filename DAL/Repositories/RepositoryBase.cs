﻿using System;
using System.Collections.Generic;
using System.Linq;
using LinqSpecs;
using Microsoft.EntityFrameworkCore;
using ServiceLayer.Gateways;

namespace DAL.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected readonly ApplicationContext _context;
        protected readonly DbSet<T> _dbSet;

        public RepositoryBase(ApplicationContext context, DbSet<T> dbSet)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _dbSet = dbSet;
        }

        public int Count(Specification<T> specification)
        {
            return _dbSet.Count(specification);
        }

        public IEnumerable<T> Find(Specification<T> specification)
        {
            return _dbSet.Where(specification).ToList();
        }
    }
}

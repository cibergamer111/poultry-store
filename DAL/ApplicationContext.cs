﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BusinessLayer;
using ServiceLayer.Gateways;
using BusinessLayer.Interfaces;

namespace DAL
{
    public class ApplicationContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<ShoppingCart> ShoppingCarts { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=poultrystoredb;Trusted_Connection=True;");
        //}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(c => (IEnumerable<Product>)c.Products)
                .WithOne(p => (Category)p.Category);

            modelBuilder.Entity<Customer>()
                .HasOne(c => (ShoppingCart)c.ShoppingCart)
                .WithOne(sc => (Customer)sc.Owner)
                .HasForeignKey<ShoppingCart>(sc => sc.OwnerId);
            modelBuilder.Entity<Customer>()
                .HasMany(c => (IEnumerable<Order>)c.Orders)
                .WithOne(o => (Customer)o.Owner);
        }
    }
}

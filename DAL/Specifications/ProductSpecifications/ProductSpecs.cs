﻿using BusinessLayer;
using BusinessLayer.Interfaces;
using DAL.Specifications.ProductSpecifications;
using LinqSpecs;
using ServiceLayer.Gateways.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Specifications
{
    public class ProductSpecs : IProductSpecs
    {
        public Specification<Product> FromCategory(ICategory category)
        {
            return new ProductFromCategorySpec(category);
        }

        public Specification<Product> FromCategory(int categoryId)
        {
            return new ProductFromCategorySpec(categoryId);
        }
    }
}

﻿using BusinessLayer;
using BusinessLayer.Interfaces;
using LinqSpecs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Specifications.ProductSpecifications
{
    public class ProductFromCategorySpec : Specification<Product>
    {
        public ICategory Category { get; }

        public int CategoryId { get; }

        public ProductFromCategorySpec(ICategory category)
        {
            this.Category = category;
        }

        public ProductFromCategorySpec(int categoryId)
        {
            this.CategoryId = categoryId;
        }

        public override Expression<Func<Product, bool>> ToExpression()
        {
            if (Category == null)
            {
                return p => p.Category.Id == CategoryId;
            }

            return p => p.Category == Category;
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace DTO.Input
{
    public record AddToCart
    {
        [Required]
        [Display(Name = nameof(ProductId))]
        public int ProductId { get; set; }

        [Required]
        [Display(Name = nameof(Amount))]
        public int Amount { get; set; }
    }
}

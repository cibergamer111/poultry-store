﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Output
{
    public record Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string InStock { get; set; }

        public string Price { get; set; }

        public string Image { get; set; }
    }
}

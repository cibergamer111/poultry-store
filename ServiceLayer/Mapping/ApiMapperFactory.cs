﻿using AutoMapper;
using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OutputDTO = DTO.Output;

namespace ServiceLayer.Mapping
{
    static class MapperFactory
    {
        private static Mapper mapper;

        static MapperFactory()
        {
            ConfigureMapper();
        }

        public static Mapper GetMapper()
        {
            return mapper;
        }

        private static void ConfigureMapper()
        {
            var config = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<Category, OutputDTO::Category>()
                        .ForMember(dto => dto.ImageLink, s => s.MapFrom(entity => entity.ImageLink));
                    cfg.CreateMap<Product, OutputDTO::Product>();
                });

            mapper = new Mapper(config);
        }
    }
}

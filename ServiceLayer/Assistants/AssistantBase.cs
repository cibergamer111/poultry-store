﻿using ServiceLayer.Gateways;

namespace ServiceLayer.Assistants
{
    public class AssistantBase
    {
        protected IUnitOfWork unitOfWork;

        public AssistantBase(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }
    }
}

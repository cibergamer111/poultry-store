﻿using System;
using BusinessLayer;
using BusinessLayer.Interfaces;
using ServiceLayer.Gateways;
using ServiceLayer.Gateways.Repositories;
using ServiceLayer.Interfaces;

namespace ServiceLayer.Assistants
{
    public class CustomerAssistant : AssistantBase, ICustomerAssistant
    {
        private readonly ICustomerRepository customerRepository;

        public CustomerAssistant(
            IUnitOfWork unitOfWork,
            ICustomerRepository customerRepository)
            : base(unitOfWork)
        {
            this.customerRepository = customerRepository;
        }

        public ICustomer GetOrCreateNewCustomerByUserId(Guid userId)
        {
            var customer = customerRepository.RetrieveByUserId(userId);

            if (customer != null)
            {
                return customer;
            }

            var newCustomer = new Customer(userId);
            customerRepository.AddCustomer(newCustomer);

            return newCustomer;
        }
    }
}

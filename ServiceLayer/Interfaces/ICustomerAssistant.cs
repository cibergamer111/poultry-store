﻿using System;
using BusinessLayer.Interfaces;

namespace ServiceLayer.Interfaces
{
    public interface ICustomerAssistant
    {
        ICustomer GetOrCreateNewCustomerByUserId(Guid userId);
    }
}

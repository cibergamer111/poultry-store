﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using InputDTO = DTO.Input;
using OutputDTO = DTO.Output;

namespace ServiceLayer.Interfaces
{
    public interface IWatchCatalogUseCase
    {
        IEnumerable<OutputDTO::Category> GetCategories();

        IEnumerable<OutputDTO::Product> GetAllProducts();

        IEnumerable<OutputDTO::Product> GetProductsByCategoryId(int categoryId);
    }
}

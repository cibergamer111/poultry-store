﻿using System;

namespace ServiceLayer.Interfaces
{
    public interface IBuyProductUseCase
    {
        void AddProductToCart(int productId, int amount, Guid userId);

        void ChangeProductAmount(int productId, int newAmount, Guid userId);

        void RemoveProductFromCart(int productId, Guid userId);

        void CreateOrder(Guid userId);
    }
}

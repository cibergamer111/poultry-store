﻿using System.Collections.Generic;
using LinqSpecs;

namespace ServiceLayer.Gateways
{
    public interface IRepositoryBase<T>
    {
        IEnumerable<T> Find(Specification<T> specification);

        int Count(Specification<T> specification);
    }
}

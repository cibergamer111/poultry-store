﻿using BusinessLayer;
using BusinessLayer.Interfaces;
using LinqSpecs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Gateways.Specifications
{
    public interface IProductSpecs
    {
        Specification<Product> FromCategory(ICategory category);

        Specification<Product> FromCategory(int categoryId);
    }
}

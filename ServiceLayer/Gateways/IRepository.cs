﻿
namespace ServiceLayer.Gateways
{
    public interface IRepository<T> : IRepositoryBase<T>
    {
        T RetrieveById(int id);
    }
}

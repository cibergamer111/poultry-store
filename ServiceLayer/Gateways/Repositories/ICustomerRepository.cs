﻿using System;
using BusinessLayer;

namespace ServiceLayer.Gateways.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        void AddCustomer(Customer customer);

        Customer RetrieveByUserId(Guid id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BusinessLayer;
using BusinessLayer.Interfaces;
using ServiceLayer.Gateways;

namespace ServiceLayer.Gateways.Repositories
{
    public interface ICategoryRepository : IRepository<Category>
    {
        IEnumerable<ICategory> RetrieveAllCategories();
    }
}

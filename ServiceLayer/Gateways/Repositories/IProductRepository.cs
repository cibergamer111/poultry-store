﻿using System.Collections.Generic;

using BusinessLayer;
using BusinessLayer.Interfaces;

namespace ServiceLayer.Gateways.Repositories
{
    public interface IProductRepository : IRepository<Product>
    {
        IEnumerable<IProduct> RetrieveAllProducts();
    }
}

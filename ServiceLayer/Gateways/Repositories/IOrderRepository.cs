﻿using BusinessLayer;
using BusinessLayer.Interfaces;

namespace ServiceLayer.Gateways.Repositories
{
    public interface IOrderRepository : IRepository<Order>
    {
        void AddOrder(IOrder order);
    }
}

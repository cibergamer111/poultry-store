﻿
using BusinessLayer;
using BusinessLayer.Interfaces;

namespace ServiceLayer.Gateways.Repositories
{
    public interface IShoppingCartRepository : IRepository<ShoppingCart>
    {
        IShoppingCart RetrieveShoppingCartByUserId(int userId);

        void DeleteShoppingCart(IShoppingCart shoppingCart);
    }
}

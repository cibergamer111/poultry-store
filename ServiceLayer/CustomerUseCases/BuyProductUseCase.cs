﻿using System;
using ServiceLayer.Gateways;
using ServiceLayer.Gateways.Repositories;
using ServiceLayer.Interfaces;

namespace ServiceLayer.CustomerUseCases
{
    public class BuyProductUseCase : UseCaseBase, IBuyProductUseCase
    {
        private readonly IProductRepository productRepository;
        private readonly IShoppingCartRepository shoppingCartRepository;
        private readonly IOrderRepository orderRepository;
        private readonly ICustomerAssistant customerAssistant;

        public BuyProductUseCase(
            IUnitOfWork unitOfWork,
            IProductRepository productRepository,
            IShoppingCartRepository shoppingCartRepository,
            IOrderRepository orderRepository,
            ICustomerAssistant customerAssistant) : base(unitOfWork)
        {
            this.productRepository = productRepository;
            this.customerAssistant = customerAssistant;
            this.shoppingCartRepository = shoppingCartRepository;
            this.orderRepository = orderRepository;
        }


        public void AddProductToCart(int productId, int amount, Guid userId)
        {
            var customer = customerAssistant.GetOrCreateNewCustomerByUserId(userId);
            var shoppingCart = customer.ShoppingCart;

            var product = productRepository.RetrieveById(productId);

            shoppingCart.AddProduct(product, amount);

            unitOfWork.Commit();
        }

        public void ChangeProductAmount(int productId, int newAmount, Guid userId)
        {
            var customer = customerAssistant.GetOrCreateNewCustomerByUserId(userId);
            var shoppingCart = customer.ShoppingCart;

            var product = productRepository.RetrieveById(productId);

            shoppingCart.ChangeProductAmount(product, newAmount);

            unitOfWork.Commit();
        }

        public void RemoveProductFromCart(int productId, Guid userId)
        {
            var customer = customerAssistant.GetOrCreateNewCustomerByUserId(userId);
            var shoppingCart = customer.ShoppingCart;

            var product = productRepository.RetrieveById(productId);

            shoppingCart.RemoveProduct(product);

            unitOfWork.Commit();
        }

        public void CreateOrder(Guid userId)
        {
            var customer = customerAssistant.GetOrCreateNewCustomerByUserId(userId);
            var shoppingCart = customer.ShoppingCart;

            var order = shoppingCart.ConvertToOrder();

            orderRepository.AddOrder(order);
            shoppingCartRepository.DeleteShoppingCart(shoppingCart);
            unitOfWork.Commit();
        }
    }
}

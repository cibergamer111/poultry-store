﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BusinessLayer;
using BusinessLayer.Interfaces;
using ServiceLayer.Gateways;
using ServiceLayer.Gateways.Repositories;
using ServiceLayer.Gateways.Specifications;
using ServiceLayer.Interfaces;
using InputDTO = DTO.Input;
using OutputDTO = DTO.Output;

namespace ServiceLayer.CustomerUseCases
{
    public class WatchCatalogUseCase : UseCaseBase, IWatchCatalogUseCase
    {
        private ICategoryRepository categoryRepository;
        private IProductRepository productRepository;
        private IProductSpecs productSpecs;

        public WatchCatalogUseCase(
            IUnitOfWork unitOfWork, 
            ICategoryRepository categoryRepository, 
            IProductRepository productRepository, 
            IProductSpecs productSpecs) 
            : base(unitOfWork)
        {
            this.categoryRepository = categoryRepository;
            this.productRepository = productRepository;
            this.productSpecs = productSpecs;
        }

        public IEnumerable<OutputDTO.Product> GetAllProducts()
        {
            var products = productRepository.RetrieveAllProducts();

            return mapper.Map<IEnumerable<OutputDTO::Product>>(products);
        }

        public IEnumerable<OutputDTO::Category> GetCategories()
        {
            var categories = categoryRepository.RetrieveAllCategories();

            return mapper.Map<IEnumerable<OutputDTO::Category>>(categories);
        }

        public IEnumerable<OutputDTO::Product> GetProductsByCategoryId(int categoryId)
        {
            var spec = productSpecs.FromCategory(categoryId);
            var productsByCategory = productRepository.Find(spec);

            return mapper.Map<IEnumerable<OutputDTO::Product>>(productsByCategory);
        }
    }
}

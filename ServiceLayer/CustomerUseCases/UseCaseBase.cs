﻿using AutoMapper;
using ServiceLayer.Gateways;
using ServiceLayer.Mapping;

namespace ServiceLayer.CustomerUseCases
{
    public abstract class UseCaseBase
    {
        protected IUnitOfWork unitOfWork;
        protected Mapper mapper;

        public UseCaseBase(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
            mapper = MapperFactory.GetMapper();
        }
    }
}

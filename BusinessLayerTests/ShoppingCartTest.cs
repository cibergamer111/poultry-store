﻿using System;
using System.Linq;
using BusinessLayer;
using BusinessLayer.Interfaces;
using FluentAssertions;
using Moq;
using Xunit;

namespace BusinessLayerTests
{
    public class ShoppingCartTest
    {
        [Fact]
        public void AddProduct_WithValidProductAndAmount_ShouldHaveCorrectCartItems()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var customer = new Customer(new Guid());
            var shoppingCart = customer.ShoppingCart;

            //Act
            shoppingCart.AddProduct(productMock.Object, amount);

            //Assert
            shoppingCart.CartItems.Should().NotBeNullOrEmpty();
            shoppingCart.CartItems.Count().Should().Be(1);

            var cartItem = shoppingCart.CartItems.First();
            cartItem.Should().NotBeNull();

            cartItem.Amount.Should().Be(amount);

            cartItem.Product.Should().NotBeNull();
            cartItem.Product.Price.Should().Be(price);
        }

        [Fact]
        public void AddProduct_WithValidProductAndAmount_ShouldHaveCorrectTotal()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;
            const decimal total = 100m;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var customer = new Customer(new Guid());
            var shoppingCart = customer.ShoppingCart;

            //Act
            shoppingCart.AddProduct(productMock.Object, amount);

            //Assert
            shoppingCart.Total.Should().Be(total);
        }

        [Fact]
        public void ChangeProductAmount_WithValidProductAndAmount_ShouldHaveCorrectTotalAndItems()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;
            const int newAmount = 5;
            const decimal total = 125m;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var customer = new Customer(new Guid());
            var shoppingCart = customer.ShoppingCart;

            shoppingCart.AddProduct(productMock.Object, amount);

            //Act
            shoppingCart.ChangeProductAmount(productMock.Object, newAmount);

            //Assert
            shoppingCart.Total.Should().Be(total);

            shoppingCart.CartItems.Should().NotBeNullOrEmpty();
            shoppingCart.CartItems.Count().Should().Be(1);

            var cartItem = shoppingCart.CartItems.First();
            cartItem.Should().NotBeNull();

            cartItem.Amount.Should().Be(newAmount);

            cartItem.Product.Should().NotBeNull();
            cartItem.Product.Price.Should().Be(price);
        }

        [Fact]
        public void RemoveProduct_WithValidProduct_ShouldHaveCorrectCartItems()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var customer = new Customer(new Guid());
            var shoppingCart = customer.ShoppingCart;

            shoppingCart.AddProduct(productMock.Object, amount);

            //Act
            shoppingCart.RemoveProduct(productMock.Object);

            //Assert
            shoppingCart.CartItems.Should().NotBeNull();
            shoppingCart.CartItems.Count().Should().Be(0);
        }

        [Fact]
        public void RemoveProduct_WithValidProduct_ShouldHaveCorrectTotal()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;
            const decimal total = 0m;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var customer = new Customer(new Guid());
            var shoppingCart = customer.ShoppingCart;

            shoppingCart.AddProduct(productMock.Object, amount);

            //Act
            shoppingCart.RemoveProduct(productMock.Object);

            //Assert
            shoppingCart.Total.Should().Be(total);
        }

        [Fact]
        public void Clear_WithoutParameters_ShouldHaveNotCartItems()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var customer = new Customer(new Guid());
            var shoppingCart = customer.ShoppingCart;

            shoppingCart.AddProduct(productMock.Object, amount);

            //Act
            shoppingCart.Clear();

            //Assert
            shoppingCart.CartItems.Should().NotBeNull();
            shoppingCart.CartItems.Count().Should().Be(0);
        }

        [Fact]
        public void Clear_WithoutParameters_ShouldHaveCorrectTotal()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;
            const decimal total = 0m;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var customer = new Customer(new Guid());
            var shoppingCart = customer.ShoppingCart;

            shoppingCart.AddProduct(productMock.Object, amount);

            //Act
            shoppingCart.Clear();

            //Assert
            shoppingCart.Total.Should().Be(total);
        }

        [Fact]
        public void ConvertToOrder_WithValidCart_ShouldReturnCorrectOrder()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;
            const decimal total = 0m;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var customer = new Customer(new Guid());
            var shoppingCart = customer.ShoppingCart;

            shoppingCart.AddProduct(productMock.Object, amount);

            //Act
            var order = shoppingCart.ConvertToOrder();

            //Assert
            order.Should().NotBeNull();
        }
    }
}

﻿using BusinessLayer;
using BusinessLayer.Interfaces;
using FluentAssertions;
using Moq;
using Xunit;

namespace BusinessLayerTests
{
    public class CartItemTest
    {
        [Fact]
        public void GetSubtotal_WithValidProductAndAmount_ShouldGetCorrectSubtotal()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;
            const decimal result = 100m;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var cartItem = new CartItem(productMock.Object, amount);

            //Act
            var subtotal = cartItem.GetSubtotal();

            //Assert
            subtotal.Should().Be(result);
        }

        [Fact]
        public void ChangeAmount_WithValidNewAmount_ShouldChangeAmount()
        {
            //Arrange
            const decimal price = 25m;
            const int amount = 4;
            const int newAmount = 5;

            var productMock = new Mock<IProduct>();
            productMock.Setup(product => product.Price)
                .Returns(price);

            var cartItem = new CartItem(productMock.Object, amount);

            //Act
            cartItem.ChangeAmount(newAmount);

            //Assert
            cartItem.Amount.Should().Be(newAmount);
        }
    }
}

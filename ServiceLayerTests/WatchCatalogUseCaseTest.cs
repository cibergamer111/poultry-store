﻿using System;
using System.Collections.Generic;
using Xunit;
using FluentAssertions;
using Moq;

using ServiceLayer.CustomerUseCases;
using ServiceLayer.Gateways;
using BusinessLayer;
using BusinessLayer.Interfaces;
using System.Linq;
using ServiceLayer.Gateways.Repositories;

namespace ServiceLayerTests
{
    public class WatchCatalogUseCaseTest
    {
        [Fact]
        public void GetCategories_WithTwoExistingCategories_ShouldGetCorrectTwoCategoryes()
        {
            //Arrange
            var categoryRepositoryMock = new Mock<ICategoryRepository>();
            categoryRepositoryMock.Setup(repo => repo.RetrieveAllCategories())
                .Returns(GetTwoTestCategoryes());

            var useCase = new WatchCatalogUseCase(null, categoryRepositoryMock.Object, null, null);

            //Act
            var categories = useCase.GetCategories();

            //Assert
            categories.Should().NotBeNullOrEmpty();
            categories.Should().HaveCount(2);

            foreach (var category in categories)
            {
                category.Should().NotBeNull();
                category.Name.Should().NotBeNullOrEmpty();
                category.Name.Should().StartWith("TestCategory");
                category.ImageLink.Should().NotBeNullOrEmpty();
                category.ImageLink.Should().StartWith("TestImageLink");
            }

            categories.First().Name.Should().Be("TestCategory1");
            categories.Last().Name.Should().Be("TestCategory2");
            categories.First().ImageLink.Should().Be("TestImageLink1");
            categories.Last().ImageLink.Should().Be("TestImageLink2");

        }

        private IEnumerable<ICategory> GetTwoTestCategoryes()
        {
            List<ICategory> categories = new()
            {
                new Category("TestCategory1", "TestImageLink1"),
                new Category("TestCategory2", "TestImageLink2")
            };

            return categories;
        }
    }
}

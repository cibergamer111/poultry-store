﻿using System;
using System.Linq;
using BusinessLayer;
using FluentAssertions;
using Moq;
using ServiceLayer.CustomerUseCases;
using ServiceLayer.Gateways;
using ServiceLayer.Gateways.Repositories;
using ServiceLayer.Interfaces;
using Xunit;

namespace ServiceLayerTests
{
    public class BuyProductUseCaseTest
    {
        [Fact]
        public void AddProductToCart_WithValidProductAndCart_ShouldHaveAddedProduct()
        {
            //Arrange
            const int _productId = 1;
            const int _amount = 2;
            const string _name = "testName";
            const decimal _price = 50m;
            const int _inStock = 15;

            var customer = new Customer(new Guid());
            var shoppingCart = customer.ShoppingCart;

            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var customerAssistantMock = new Mock<ICustomerAssistant>();
            customerAssistantMock.Setup(ass => ass.GetOrCreateNewCustomerByUserId(It.IsAny<Guid>()))
                .Returns(customer);

            var productRepositoryMock = new Mock<IProductRepository>();
            productRepositoryMock.Setup(repo => repo.RetrieveById(It.IsAny<int>()))
                .Returns(new Product(_name, _price, _inStock));

            var shoppingCartRepositoryMock = new Mock<IShoppingCartRepository>();
            shoppingCartRepositoryMock.Setup(repo => repo.RetrieveShoppingCartByUserId(It.IsAny<int>()))
                .Returns(shoppingCart);

            var useCase = new BuyProductUseCase(
                unitOfWorkMock.Object,
                productRepositoryMock.Object,
                null,
                null,
                customerAssistantMock.Object);

            //Act
            useCase.AddProductToCart(_productId, _amount, customer.UserId);

            //Assert
            shoppingCart.CartItems.Should().NotBeNullOrEmpty();
            shoppingCart.CartItems.Count().Should().Be(1);
            shoppingCart.CartItems.First().Product.Should().NotBeNull();
            shoppingCart.CartItems.First().Product.Name.Should().Be(_name);
            shoppingCart.CartItems.First().Amount.Should().Be(_amount);
        }

        //[Fact]
        //public void AddProductToCart_WithNullCart_ShouldHaveAddedProduct()
        //{
        //    //Arrange
        //    const int _productId = 1;
        //    const int _amount = 2;
        //    const string _name = "testName";
        //    const decimal _price = 50m;
        //    const int _inStock = 15;

        //    var productRepositoryMock = new Mock<IProductRepository>();
        //    productRepositoryMock.Setup(gateway => gateway.RetrieveProductById(It.IsAny<int>()))
        //        .Returns(new Product(_name, _price, _inStock));

        //    var shoppingCartRepositoryMock = new Mock<IShoppingCartRepository>();
        //    shoppingCartRepositoryMock.Setup(gateway => gateway.RetrieveShoppingCartByUserId(It.IsAny<int>()))
        //        .Returns(null);

        //    var useCase = new BuyProductUseCase(
        //        productRepositoryMock.Object,
        //        shoppingCartRepositoryMock.Object);

        //    var apiDTOProduct = new ApiDTO::Product() { Id = _productId };

        //    //Act
        //    useCase.AddProductToCart(apiDTOProduct, _amount);

        //    //Assert
        //    shoppingCartRepositoryMock.Verify(
        //        gw => gw.UpdateShoppingCart(It.Is<DalDTO::ShoppingCart>(
        //        sc => sc.CartItems.Count() == 1)));

        //    shoppingCartRepositoryMock.Verify(
        //        gw => gw.UpdateShoppingCart(It.Is<DalDTO::ShoppingCart>(
        //        sc => sc.CartItems.First().Product.Price == _price)));

        //    shoppingCartRepositoryMock.Verify(
        //        gw => gw.UpdateShoppingCart(It.Is<DalDTO::ShoppingCart>(
        //        sc => sc.CartItems.First().Amount == _amount)));
        //}
    }
}

﻿namespace BusinessLayer.Interfaces
{
    public interface ICartItem
    {
        IProduct Product { get; }

        int Amount { get; }

        decimal GetSubtotal();

        void ChangeAmount(int newAmount);
    }
}

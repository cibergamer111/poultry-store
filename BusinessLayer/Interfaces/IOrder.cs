﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface IOrder
    {
        int Id { get; }

        IEnumerable<ICartItem> CartItems { get; }

        decimal Total { get; }

        ICustomer Owner { get; }
    }
}

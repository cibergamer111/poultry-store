﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface ICustomer
    {
        int Id { get; }

        Guid UserId { get; }

        IShoppingCart ShoppingCart { get; }

        IEnumerable<IOrder> Orders { get; }
    }
}

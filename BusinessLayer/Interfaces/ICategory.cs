﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface ICategory
    {
        int Id { get; }

        string Name { get; }

        string ImageLink { get; }

        IEnumerable<IProduct> Products { get; }

        void AddProduct();

        void RemoveProduct();

        IProduct GetProductByName(string productName);

        IEnumerable<IProduct> GetProducts();
    }
}

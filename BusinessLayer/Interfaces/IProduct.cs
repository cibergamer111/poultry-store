﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface IProduct
    {
        int Id { get; }

        string Name { get; }

        int InStock { get; }

        decimal Price { get; }

        string Image { get; }

        ICategory Category { get; }

    }
}

﻿using System.Collections.Generic;

namespace BusinessLayer.Interfaces
{
    public interface IShoppingCart
    {
        int Id { get; }

        IEnumerable<ICartItem> CartItems { get; }

        decimal Total { get; }

        public int OwnerId { get; }

        ICustomer Owner { get; }

        void AddProduct(IProduct product, int amount);

        void ChangeProductAmount(IProduct product, int newAmount);

        void RemoveProduct(IProduct product);

        void Clear();

        IOrder ConvertToOrder();
    }
}

﻿using BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Order : IOrder
    {
        private Order()
        {
        }

        public Order(IShoppingCart shoppingCart)
        {
            _cartItems = shoppingCart.CartItems.ToList();
            Total = shoppingCart.Total;
        }

        public int Id { get; private set; }

        private readonly IList<ICartItem> _cartItems;

        public IEnumerable<ICartItem> CartItems => _cartItems;

        public decimal Total { get; private set; }

        public ICustomer Owner { get; private set; }
    }
}

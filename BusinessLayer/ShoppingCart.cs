﻿using System.Collections.Generic;
using System.Linq;
using BusinessLayer.Interfaces;

namespace BusinessLayer
{
    public class ShoppingCart : IShoppingCart
    {
        private ShoppingCart()
        {
        }

        public ShoppingCart(ICustomer customer)
        {
            _cartItems = new List<ICartItem>();
            this.Owner = customer;
        }

        public int Id { get; private set; }

        private readonly IList<ICartItem> _cartItems;

        public IEnumerable<ICartItem> CartItems => _cartItems;

        public decimal Total { get; private set; }

        public int OwnerId { get; private set; }

        public ICustomer Owner { get; private set; }

        public void AddProduct(IProduct product, int amount)
        {
            var cartItem = new CartItem(product, amount);
            _cartItems.Add(cartItem);

            Total += cartItem.GetSubtotal();
        }

        public void ChangeProductAmount(IProduct product, int newAmount)
        {
            var item = _cartItems.First(item => item.Product.Id == product.Id);
            Total -= item.GetSubtotal();

            item.ChangeAmount(newAmount);
            Total += item.GetSubtotal();
        }

        public void RemoveProduct(IProduct product)
        {
            var item = _cartItems.First(item => item.Product.Id == product.Id);
            _cartItems.Remove(item);

            Total -= item.GetSubtotal();
        }

        public void Clear()
        {
            _cartItems.Clear();
            Total = default;
        }

        public IOrder ConvertToOrder()
        {
            return new Order(this);
        }
    }
}

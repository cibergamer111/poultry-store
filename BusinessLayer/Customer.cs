﻿using System;
using System.Collections.Generic;
using BusinessLayer.Interfaces;

namespace BusinessLayer
{
    public class Customer : ICustomer
    {
        public int Id { get; private set; }

        public Guid UserId { get; private set; }

        public IShoppingCart ShoppingCart { get; private set; }

        public IEnumerable<IOrder> Orders { get; private set; }

        public Customer(Guid userId)
        {
            this.UserId = userId;
            this.ShoppingCart = new ShoppingCart(this);
        }
    }
}

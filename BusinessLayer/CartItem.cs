﻿using BusinessLayer.Interfaces;

namespace BusinessLayer
{
    public class CartItem : ICartItem
    {
        public CartItem(IProduct product, int amount)
        {
            this.Product = product;
            this.Amount = amount;
        }

        public IProduct Product { get; private set; }

        public int Amount { get; private set; }

        public void ChangeAmount(int newAmount)
        {
            Amount = newAmount;
        }

        public decimal GetSubtotal()
        {
            return Product.Price * Amount;
        }
    }
}

﻿using BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Category : ICategory
    {
        private Category()
        {
        }

        public Category(string name, string image)
        {
            this.Name = name;
            this.ImageLink = image;

            this._products = new List<IProduct>();
        }

        public int Id { get; private set; }

        public string Name { get; private set; }

        public string ImageLink { get; private set; }

        private readonly IList<IProduct> _products;

        public IEnumerable<IProduct> Products => _products;

        public void AddProduct()
        {
            throw new NotImplementedException();
        }

        public IProduct GetProductByName(string productName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IProduct> GetProducts()
        {
            throw new NotImplementedException();
        }

        public void RemoveProduct()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using BusinessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Product : IProduct
    {
        private Product()
        {
        }

        public Product(string name, decimal price, int inStock)
        {
            this.Name = name;
            this.Price = price;
            this.InStock = inStock;
        }

        public int Id { get; protected set; }

        public string Name { get; protected set; }

        public int InStock { get; protected set; }

        public decimal Price { get; protected set; }

        public string Image { get; protected set; }

        public ICategory Category { get; protected set; }
    }
}

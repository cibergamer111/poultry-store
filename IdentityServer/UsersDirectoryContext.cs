﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IdentityServer
{
    public class UsersDirectoryContext : IdentityDbContext<User>
    {
        private const string _connectionString
            = "Server=(localdb)\\mssqllocaldb;Database=usersdirectorydb;Trusted_Connection=True;";

        public UsersDirectoryContext(
            DbContextOptions<UsersDirectoryContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}

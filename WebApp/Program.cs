using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using WebApp.ApiClients;
using WebApp.ApiClients.Interfaces;
using WebApp.Landing;
using WebApp.Landing.Interfaces;
using WebApp.Showcase;
using WebApp.Showcase.Interfaces;

namespace WebApp
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            //builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri("https://localhost:44317") });

            builder.Services.AddScoped<IAuthorizationClient, AuthorizationClient>();
            builder.Services.AddScoped<ICategoryClient, CategoryClient>();
            builder.Services.AddScoped<IProductClient, ProductClient>();

            builder.Services.AddScoped<ICarouselPresenter, CarouselPresenter>();
            builder.Services.AddScoped<ICategoryListingPresenter, CategoryListingPresenter>();
            builder.Services.AddScoped<ILoginPresenter, LoginPresenter>();
            builder.Services.AddScoped<IProductListingPresenter, ProductListingPresenter>();

            await builder.Build().RunAsync();
        }
    }
}

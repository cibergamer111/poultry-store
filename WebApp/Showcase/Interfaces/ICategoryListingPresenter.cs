﻿using System.Threading.Tasks;

namespace WebApp.Showcase.Interfaces
{
    public interface ICategoryListingPresenter
    {
        Task<CategoryListingViewModel> InitAndGetViewModel();

        Task UpdateCategories();
    }
}
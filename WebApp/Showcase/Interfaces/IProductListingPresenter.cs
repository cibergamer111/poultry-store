﻿using System.Threading.Tasks;

namespace WebApp.Showcase.Interfaces
{
    public interface IProductListingPresenter
    {
        Task<ProductListingViewModel> InitAndGetViewModel();

        Task UpdateProducts();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.ApiClients;
using AutoMapper;
using System.Net.Http;
using WebApp.ApiClients.Interfaces;
using WebApp.Showcase.Interfaces;

namespace WebApp.Showcase
{
    public class CategoryListingPresenter : ICategoryListingPresenter
    {
        private readonly ICategoryClient categoryClient;
        private CategoryListingViewModel viewModel;
        private Mapper mapper;

        public CategoryListingPresenter(ICategoryClient categoryClient)
        {
            this.categoryClient = categoryClient;
            this.viewModel = new CategoryListingViewModel();
            ConfigureMapper();
        }

        public async Task<CategoryListingViewModel> InitAndGetViewModel()
        {
            await UpdateCategories();
            return viewModel;
        }

        public async Task UpdateCategories()
        {
            try
            {
                var categories = await categoryClient.ReceiveAllCategories();
                MapCategoriesToViewModel(categories);
            }
            catch (HttpRequestException)
            {
                //TODO: add logs.
            }
        }

        private void MapCategoriesToViewModel(IList<CategoryDTO> dtoList)
        {
            List<Category> categoryes = new()
            {
                new() { Label = "Все категории", IsActive = true }
            };

            categoryes.AddRange(mapper.Map<IList<CategoryDTO>, List<Category>>(dtoList));
            viewModel.Categoryes = categoryes;
        }

        private void ConfigureMapper()
        {
            var config = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<CategoryDTO, Category>()
                        .ForMember(vm => vm.Label, s => s.MapFrom(dto => dto.Name));
                    //To map another DTO to his ViewModel - just add new map.
                });

            mapper = new Mapper(config);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Showcase
{
    public class Product
    {
        public string InStock { get; set; }

        public string Image { get; set; }

        public string Name { get; set; }

        public string Price { get; set; }

        public string DefaultAmount { get; set; } = "23";

        public string Amount { get; set; }
    }
}

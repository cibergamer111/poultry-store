﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Showcase
{
    public class Category
    {
        public string Label { get; set; }

        public bool IsActive { get; set; }
    }
}

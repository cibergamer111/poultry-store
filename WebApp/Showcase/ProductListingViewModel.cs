﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.ApiClients;
using AutoMapper;
using System.Net.Http;

namespace WebApp.Showcase
{
    public class ProductListingViewModel
    {
        public List<Product> Products { get; set; }

        public ProductListingViewModel()
        {
            Products = new List<Product>();
        }
    }
}

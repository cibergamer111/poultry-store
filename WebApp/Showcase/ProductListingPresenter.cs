﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.ApiClients;
using AutoMapper;
using System.Net.Http;
using WebApp.ApiClients.Interfaces;
using WebApp.Showcase.Interfaces;

namespace WebApp.Showcase
{
    public class ProductListingPresenter : IProductListingPresenter
    {
        private readonly IProductClient productClient;
        private ProductListingViewModel viewModel;
        private Mapper mapper;

        public ProductListingPresenter(IProductClient productClient)
        {
            this.productClient = productClient;
            this.viewModel = new ProductListingViewModel();
            ConfigureMapper();
        }

        public async Task<ProductListingViewModel> InitAndGetViewModel()
        {
            await UpdateProducts();
            return viewModel;
        }

        public async Task UpdateProducts()
        {
            try
            {
                var products = await productClient.ReceiveAllProducts();
                MapProductsToViewModel(products);
            }
            catch (HttpRequestException)
            {
                //TODO: add logs.
            }
        }

        private void MapProductsToViewModel(IList<ProductDTO> dtoList)
        {
            var products = mapper.Map<IList<ProductDTO>, List<Product>>(dtoList);
            viewModel.Products = products;
        }

        private void ConfigureMapper()
        {
            var config = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<ProductDTO, Product>();
                    //To map another DTO to his ViewModel - just add new map.
                });

            mapper = new Mapper(config);
        }
    }
}

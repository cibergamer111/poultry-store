﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Showcase
{
    public class CategoryListingViewModel
    {
        public List<Category> Categoryes { get; set; }

        public CategoryListingViewModel()
        {
            Categoryes = new List<Category>();
        }
    }
}

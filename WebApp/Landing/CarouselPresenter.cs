﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.ApiClients;
using AutoMapper;
using System.Net.Http;
using WebApp.ApiClients.Interfaces;
using WebApp.Landing.Interfaces;

namespace WebApp.Landing
{
    public class CarouselPresenter : ICarouselPresenter
    {
        private readonly ICategoryClient categoryClient;
        private CarouselViewModel viewModel;
        private Mapper mapper;

        public CarouselPresenter(ICategoryClient categoryClient)
        {
            this.categoryClient = categoryClient;
            this.viewModel = new CarouselViewModel();
            ConfigureMapper();
        }

        public async Task<CarouselViewModel> InitAndGetViewModel()
        {
            await UpdateCategories();
            return viewModel;
        }

        public async Task UpdateCategories()
        {
            try
            {
                var categories = await categoryClient.ReceiveAllCategories();
                MapCategoriesToViewModel(categories);
            }
            catch (HttpRequestException)
            {
                //TODO: add logs.
            }
        }

        private void MapCategoriesToViewModel(IList<CategoryDTO> dtoList)
        {
            var categoryes = mapper.Map<IList<CategoryDTO>, List<CarouselItemViewModel>>(dtoList);
            viewModel.CarouselItems = categoryes;
        }

        private void ConfigureMapper()
        {
            var config = new MapperConfiguration(
                cfg =>
                {
                    cfg.CreateMap<CategoryDTO, CarouselItemViewModel>()
                        .ForMember(vm => vm.Label, s => s.MapFrom(dto => dto.Name));
                    //To map another DTO to his ViewModel - just add new map.
                });

            mapper = new Mapper(config);
        }
    }
}

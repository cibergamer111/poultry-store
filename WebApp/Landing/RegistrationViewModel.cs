﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Landing
{
    public class RegistrationViewModel
    {
        [Required (ErrorMessage = "Имя обязательно")]
        [Display(Name = nameof(Name))]
        public string Name { get; set; }

        [Required(ErrorMessage = "Телефон обязателен")]
        [Display(Name = nameof(Phone))]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Пароль обязателен")]
        [Display(Name = nameof(Password))]
        public string Password { get; set; }

        [Required(ErrorMessage = "Повторите пароль")]
        [Compare(nameof(Password), ErrorMessage = "Пароли не совпадают")]
        [Display(Name = nameof(ConfirmPassword))]
        public string ConfirmPassword { get; set; }

        [Display(Name = nameof(RememberMe))]
        public bool RememberMe { get; set; }
    }
}

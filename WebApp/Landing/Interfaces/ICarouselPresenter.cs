﻿using System.Threading.Tasks;

namespace WebApp.Landing.Interfaces
{
    public interface ICarouselPresenter
    {
        Task<CarouselViewModel> InitAndGetViewModel();

        Task UpdateCategories();
    }
}
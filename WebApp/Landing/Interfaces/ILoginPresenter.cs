﻿using System.Threading.Tasks;

namespace WebApp.Landing.Interfaces
{
    public interface ILoginPresenter
    {
        Task<(LoginViewModel, RegistrationViewModel)> InitAndGetViewModel();

        Task LoginUser();

        Task RegisterUser();
    }
}

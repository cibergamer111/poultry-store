﻿using WebApp.Landing.Interfaces;
using System.Threading.Tasks;
using WebApp.ApiClients.Interfaces;
using System;

namespace WebApp.Landing
{
    public class LoginPresenter : ILoginPresenter
    {
        private readonly IAuthorizationClient authorizationClient;
        private LoginViewModel loginViewModel;
        private RegistrationViewModel registrationViewModel;

        public LoginPresenter(IAuthorizationClient authorizationClient)
        {
            this.authorizationClient = authorizationClient;
            this.loginViewModel = new LoginViewModel();
            this.registrationViewModel = new RegistrationViewModel();
        }

        public async Task<(LoginViewModel, RegistrationViewModel)> InitAndGetViewModel()
        {
            return (loginViewModel, registrationViewModel);
        }

        public async Task LoginUser()
        {
            await authorizationClient.LoginUser(loginViewModel.Phone, loginViewModel.Password);
        }

        public async Task RegisterUser()
        {
            throw new NotImplementedException();
        }
    }
}

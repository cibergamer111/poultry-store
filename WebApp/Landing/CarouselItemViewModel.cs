﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.Landing
{
    public class CarouselItemViewModel
    {
        public string ImageLink { get; set; }
        public string Label { get; set; }
    }
}

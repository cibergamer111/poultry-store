﻿using System.ComponentModel.DataAnnotations;

namespace WebApp.Landing
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Телефон обязателен")]
        [Display(Name = nameof(Phone))]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Пароль обязателен")]
        [Display(Name = nameof(Password))]
        public string Password { get; set; }

        [Display(Name = nameof(RememberMe))]
        public bool RememberMe { get; set; }
    }
}

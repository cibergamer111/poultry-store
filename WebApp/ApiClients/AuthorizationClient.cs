﻿using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Security.Claims;
using System.Threading.Tasks;
using WebApp.ApiClients.Interfaces;

namespace WebApp.ApiClients
{
    public class AuthorizationClient : IAuthorizationClient
    {
        private readonly string _identityServerUrl = "https://localhost:5001";
        private readonly HttpClient _httpClient;
        private readonly HttpClient _applicationHttpClient;
        private DiscoveryDocumentResponse discoveryDocument;
        private string accessToken;

        public AuthorizationClient(HttpClient httpClient)
        {
            this._httpClient = httpClient;
            this._applicationHttpClient = new HttpClient();
        }

        private async Task SetDiscoveryDocument()
        {
            try
            {
                discoveryDocument = await _applicationHttpClient.GetDiscoveryDocumentAsync(_identityServerUrl);
            }
            catch (Exception ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task LoginUser(string userName, string password)
        {
            await SetDiscoveryDocument();

            try
            {
                var tokenResponse = await _applicationHttpClient.RequestPasswordTokenAsync(new PasswordTokenRequest
                {
                    Address = discoveryDocument.TokenEndpoint,
                    ClientId = "client",
                    ClientSecret = "secret",

                    Scope = "PoultryStoreWebAPI openid",
                    UserName = userName,
                    Password = password
                });

                if (tokenResponse.IsError)
                {
                    throw new HttpRequestException(tokenResponse.Error);
                }

                accessToken = tokenResponse.AccessToken;
                _httpClient.SetBearerToken(tokenResponse.AccessToken);
            }
            catch (Exception ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task<IEnumerable<Claim>> GetUserInfo(string userName, string password)
        {
            try
            {
                var userInfoResponse = await _applicationHttpClient.GetUserInfoAsync(new UserInfoRequest
                {
                    Address = discoveryDocument.UserInfoEndpoint,
                    ClientId = "client",
                    ClientSecret = "secret",

                    Token = accessToken
                });

                if (userInfoResponse.IsError)
                {
                    throw new HttpRequestException(userInfoResponse.Error);
                }

                return userInfoResponse.Claims;
            }
            catch (Exception ex)
            {
                throw new HttpRequestException(ex.Message);
            }
        }

        public async Task RegisterUser()
        {
            throw new NotImplementedException();
        }

        public async Task LogoutUser()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApp.ApiClients.Interfaces
{
    public interface IProductClient
    {
        Task<IList<ProductDTO>> ReceiveAllProducts();

        Task<IList<ProductDTO>> ReceiveProductsByCategoryId(int categoryId);
    }
}

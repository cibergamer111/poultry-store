﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApp.ApiClients.Interfaces
{
    public interface ICategoryClient
    {
        Task<IList<CategoryDTO>> ReceiveAllCategories();
    }
}

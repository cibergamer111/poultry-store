﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace WebApp.ApiClients.Interfaces
{
    public interface IAuthorizationClient
    {
        Task<IEnumerable<Claim>> GetUserInfo(string userName, string password);

        Task LoginUser(string userName, string password);

        Task LogoutUser();

        Task RegisterUser();
    }
}
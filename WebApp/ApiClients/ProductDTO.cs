﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp.ApiClients
{
    public record ProductDTO
    {
        public string InStock { get; set; }

        public string Image { get; set; }

        public string Name { get; set; }

        public string Price { get; set; }
    }
}

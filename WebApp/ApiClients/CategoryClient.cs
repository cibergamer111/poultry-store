﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using WebApp.ApiClients.Interfaces;

namespace WebApp.ApiClients
{
    public class CategoryClient : ICategoryClient
    {
        private readonly HttpClient _httpClient;

        public CategoryClient(HttpClient httpClient)
        {
            this._httpClient = httpClient;
        }

        public async Task<IList<CategoryDTO>> ReceiveAllCategories()
        {
            try
            {
                //var response = await httpClient.GetFromJsonAsync<List<CategoryDTO>>("sample-data/categoryes.json");
                var response = await _httpClient.GetFromJsonAsync<List<CategoryDTO>>("/category");
                return response;
            }
            catch
            {
                throw new HttpRequestException();
            }
        }
    }
}

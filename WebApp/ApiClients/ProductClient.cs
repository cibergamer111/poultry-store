﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using WebApp.ApiClients.Interfaces;

namespace WebApp.ApiClients
{
    public class ProductClient : IProductClient
    {
        private readonly HttpClient _httpClient;

        public ProductClient(HttpClient httpClient)
        {
            this._httpClient = httpClient;
        }

        public async Task<IList<ProductDTO>> ReceiveAllProducts()
        {
            try
            {
                var response = await _httpClient.GetFromJsonAsync<List<ProductDTO>>($"/product");
                return response;
            }
            catch
            {
                throw new HttpRequestException();
            }
        }

        public async Task<IList<ProductDTO>> ReceiveProductsByCategoryId(int categoryId)
        {
            try
            {
                var response = await _httpClient.GetFromJsonAsync<List<ProductDTO>>($"/productByCategoryId/{categoryId}");
                return response;
            }
            catch
            {
                throw new HttpRequestException();
            }
        }
    }
}

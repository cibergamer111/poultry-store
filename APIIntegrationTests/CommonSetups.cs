﻿using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using API;
using DAL;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace APIIntegrationTests
{
    internal static class CommonSetups
    {
        public static WebApplicationFactory<Startup> CreateWebHostFactory()
        {
            var webHostFactory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureTestServices(services =>
                    {
                        services.ConfigureDatabase();
                        services.ConfigureAuth();
                    });

                    builder.UseEnvironment("Testing");
                });

            return webHostFactory;
        }

        public static ApplicationContext GetContext(this WebApplicationFactory<Startup> webHostFactory)
        {
            var context = webHostFactory.Services.CreateScope().ServiceProvider.GetService<ApplicationContext>();

            return context ?? throw new ApplicationException($"dbContext '{nameof(ApplicationContext)}' not created.");
        }

        public static HttpClient CreateAuthorizedClient(this WebApplicationFactory<Startup> webHostFactory)
        {
            var httpClient = webHostFactory.CreateClient();
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Test");

            return httpClient;
        }

        private static void ConfigureDatabase(this IServiceCollection services)
        {
            services.RemoveService<DbContextOptions<ApplicationContext>>();

            services.AddDbContext<ApplicationContext>(options =>
            {
                options.UseInMemoryDatabase("test_db");
            });
        }

        private static void ConfigureAuth(this IServiceCollection services)
        {
            services.RemoveService<IAuthenticationService>();
            services.RemoveService<IAuthorizationService>();

            services.AddAuthentication("Test")
                .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>(
                    "Test", options => { });

            services.AddAuthorization();
        }

        private static void RemoveService<T>(this IServiceCollection services)
        {
            var contextDescriptor = services.SingleOrDefault(d =>
                            d.ServiceType == typeof(T));

            if (contextDescriptor == null)
            {
                var typeName = typeof(T).Name;
                var typeArguments = string.Join<Type>(", ", typeof(T).GenericTypeArguments);

                if (!string.IsNullOrEmpty(typeArguments))
                {
                    typeArguments = $"<{typeArguments}>";
                }

                throw new ApplicationException($"Context descriptor not found, var name: '{typeName}{typeArguments}'");
            }

            services.Remove(contextDescriptor);
        }
    }
}

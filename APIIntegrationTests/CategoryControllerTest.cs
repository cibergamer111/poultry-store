﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using BusinessLayer;
using FluentAssertions;
using Xunit;
using OutputDTO = DTO.Output;

namespace APIIntegrationTests
{
    public class CategoryControllerTest
    {
        [Fact]
        public async Task Get_WithValidData_ShouldReturnValidCategories()
        {
            //Arrange
            var webHostFactory = CommonSetups.CreateWebHostFactory();
            var context = webHostFactory.GetContext();

            List<Category> categories = new()
            {
                new Category("testName1", "testImage1"),
                new Category("testName2", "testImage2"),
                new Category("testName3", "testImage3")
            };

            await context.Categories.AddRangeAsync(categories);
            await context.SaveChangesAsync();

            var httpClient = webHostFactory.CreateClient();

            //Act
            var response = await httpClient.GetAsync("Category/");

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var dto = await response.ParseAsync<IEnumerable<OutputDTO::Category>>();

            dto.Should().NotBeNull();
            dto.Should().HaveCount(categories.Count);
            dto.First().Name.Should().Be("testName1");
            dto.First().ImageLink.Should().Be("testImage1");
        }
    }
}

﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace APIIntegrationTests
{
    internal static class CommonParser
    {
        public static async Task<T> ParseAsync<T>(this HttpResponseMessage response)
        {
            try
            {
                var json = await response.Content.ReadAsStringAsync();
                var dto = JsonSerializer.Deserialize<T>(json, new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });

                return dto ?? throw new ApplicationException(
                    $"Deserialization failed, returned parameter: 'null'.");
            }
            catch (Exception ex)
            {
                throw new ApplicationException(
                $"Deserialization failed with message: {ex.Message}");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using BusinessLayer;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;
using InputDTO = DTO.Input;

namespace APIIntegrationTests
{
    public class ShoppingCartControllerTest : IDisposable
    {
        private readonly WebApplicationFactory<API.Startup> webHostFactory;
        private readonly DAL.ApplicationContext context;

        public ShoppingCartControllerTest()
        {
            this.webHostFactory = CommonSetups.CreateWebHostFactory();
            this.context = webHostFactory.GetContext();
        }

        public void Dispose()
        {
            context.Database.EnsureDeletedAsync();
        }

        [Fact]
        public async Task AddProductToCart_WithUnexistCustomer_ShouldAddProductInDb()
        {
            //Arrange
            List<Product> products = new()
            {
                new Product("prod1", 1m, 5),
                new Product("prod2", 2m, 6),
                new Product("prod3", 3m, 7)
            };

            await context.Products.AddRangeAsync(products);
            await context.SaveChangesAsync();

            var productId = context.Products.First(p => p.Name == "prod2").Id;

            InputDTO.AddToCart input = new()
            {
                Amount = 5,
                ProductId = productId
            };

            var inputJson = JsonSerializer.Serialize(input);

            var httpClient = webHostFactory.CreateAuthorizedClient();

            //Act
            var response = await httpClient.PostAsync(
                "ShoppingCart/",
                new StringContent(inputJson, Encoding.Default, MediaTypeNames.Application.Json));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var product = context.Products.First(p => p.Id == productId);

            product.Name.Should().Be("prod2");
            product.Price.Should().Be(2m);
            product.InStock.Should().Be(6);

            var customer = context.Customers.First();

            customer.Should().NotBeNull();
            customer.ShoppingCart.Should().NotBeNull();

            //TODO: add ability to retrieve nested properties from database.

            //customer.ShoppingCart.Total.Should().Be(2m);

            //TODO: add saving CartItems to the database.

            //customer.ShoppingCart.CartItems.Count().Should().Be(1);
            //customer.ShoppingCart.CartItems.First().Amount.Should().Be(5);
            //customer.ShoppingCart.CartItems.First().Product.Name.Should().Be("prod2");
            //customer.ShoppingCart.CartItems.First().Product.Price.Should().Be(2m);
            //customer.ShoppingCart.CartItems.First().Product.InStock.Should().Be(6);
        }

        [Fact]
        public async Task AddProductToCart_WithExistingCustomer_ShouldNotCreateAnotherCustomer()
        {
            //Arrange
            List<Product> products = new()
            {
                new Product("prod1", 1m, 5),
                new Product("prod2", 2m, 6),
                new Product("prod3", 3m, 7)
            };

            await context.Products.AddRangeAsync(products);

            context.Customers.Add(
                new Customer(TestAuthHandler.UserId));

            await context.SaveChangesAsync();

            var productId = context.Products.First(p => p.Name == "prod2").Id;

            InputDTO.AddToCart input = new()
            {
                Amount = 5,
                ProductId = productId
            };

            var inputJson = JsonSerializer.Serialize(input);

            var httpClient = webHostFactory.CreateAuthorizedClient();

            //Act
            var response = await httpClient.PostAsync(
                "ShoppingCart/",
                new StringContent(inputJson, Encoding.Default, MediaTypeNames.Application.Json));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            context.Customers.Count().Should().Be(1);
            var customer = context.Customers.First();

            customer.ShoppingCart.Should().NotBeNull();
            customer.UserId.Should().Be(TestAuthHandler.UserId);
        }
    }
}
